exo 2.2

Annuler le dernier commit en conservant les modifications:
git reset HEAD^

Annuler le dernier commit en PERDANT les modifications:
git reset --hard HEAD^