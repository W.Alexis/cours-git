git rebase -i master

Les conflits lors d’un rebase sont à résoudre au fur et à mesure du replay

On résout un conflit de rebase avec 
git add fichier_en_conflit...
git rebase --continue

Abandon du rebase avec 
git rebase --abort